package mx.tecnm.misantla.tacoappmisantla

import android.graphics.Bitmap
import android.graphics.Canvas
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.floatingactionbutton.FloatingActionButton
import mx.tecnm.misantla.tacoappmisantla.databinding.ActivityMapsBinding

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap

    private val taquerias = mutableListOf<Taqueria>()
    private lateinit var myLocationButton : FloatingActionButton
    private lateinit var binding: ActivityMapsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)

       addTaqueria()
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    private fun addTaqueria() {
        taquerias.add(Taqueria("Taqueria Estrella",19.934618297282906,-96.84890002012256))
        taquerias.add(Taqueria("Taqueria El Compa",19.931103291068656,-96.8522796034813))
        taquerias.add(Taqueria("Taqueria La Parroquia II", 19.92877335863342,-96.85337394475938))
        taquerias.add(Taqueria("Taqueria Raquel",19.928571631265534,-96.85337930917741))
        taquerias.add(Taqueria("Taqueria La Parroquia",19.928223650950734,-96.85451656579973))
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        val icon = getTacoIcon()

        for (taqueria in taquerias){
            val taqueriaPosition = LatLng(taqueria.latitud,taqueria.longitud)
            val taqueriaName = taqueria.name

            val markerOptions = MarkerOptions().position(taqueriaPosition).title(taqueriaName)
                    .icon(icon)
            mMap.addMarker(markerOptions)
        }

        // Add a marker in Sydney and move the camera
        val ITSM = LatLng(19.950699587746577, -96.84399962425232)
       // val taqueria = LatLng(19.934618297282906,-96.84890002012256)
        mMap.addMarker(MarkerOptions().position(ITSM).title("Marker in TecMisantla"))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(ITSM))

      //  mMap.addMarker(MarkerOptions().position(taqueria).title("Marker in Taqueria Estrella"))
        binding.myLocation.setOnClickListener {
         mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(ITSM,13.0f))

        }

    }

    private fun getTacoIcon(): BitmapDescriptor {
        val drawable = ContextCompat.getDrawable(this, R.drawable.taco_3)
        drawable?.setBounds(0,0,drawable.intrinsicWidth,drawable.intrinsicHeight)
        val bitmap = Bitmap.createBitmap(drawable?.intrinsicWidth ?: 0,
                drawable?.intrinsicHeight ?: 0, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        drawable?.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)

    }
}